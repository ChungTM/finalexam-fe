export interface Qoute {
    weight: number;
    price: number;
    fromCountry: string;
    toCountry: string;
}
