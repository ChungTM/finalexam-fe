import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateShipmentComponent } from './shipment/create-shipment/create-shipment.component';
import { HttpClientModule } from '@angular/common/http';
import { ShipmentComponent } from './shipment/shipment/shipment.component';
import { GetShipmentComponent } from './shipment/get-shipment/get-shipment.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateShipmentComponent,
    ShipmentComponent,
    GetShipmentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
