import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ShipmentService } from 'src/app/service/shipment.service';
import { Qoute } from 'src/app/qoute';
import { RefId } from 'src/app/ref-id';

@Component({
  selector: 'app-create-shipment',
  templateUrl: './create-shipment.component.html',
  styleUrls: ['./create-shipment.component.css']
})
export class CreateShipmentComponent implements OnInit {
  quote: Qoute;
  result: RefId;
  createShipmentForm = this.fb.group({
    contact: this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      countryCode: ['FR', Validators.required],
      locality: ['', Validators.required],
      postalCode: ['', Validators.required],
      address: ['', Validators.required],
    }),
    destination: this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      countryCode: ['FR', Validators.required],
      locality: ['', Validators.required],
      postalCode: ['', Validators.required],
      address: ['', Validators.required],
    }),
    package: this.fb.group({
      dimension: this.fb.group({
        height: ['', Validators.required],
        width: ['', Validators.required],
        length: ['', Validators.required],
        unit: ['cm', Validators.required],
      }),
      grossWeight: this.fb.group({
        amount: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
        unit: ['Kg', Validators.required]
      })
    })
  });
  constructor(private fb: FormBuilder,
              private shipmentService: ShipmentService) {
  }

  ngOnInit() {
  }
  getQuote(formValue) {
    this.result = null;
    const quotePrice = {
      weight: formValue.package.grossWeight.amount,
      fromCountry: formValue.contact.countryCode.toUpperCase(),
      toCountry: formValue.contact.countryCode.toUpperCase()
    };
    if (quotePrice.weight > 0 && quotePrice.fromCountry === 'FR' && quotePrice.toCountry === 'FR') {
      return this.shipmentService.getQuote(quotePrice).subscribe(
        res => this.quote = res,
        err => { if (err) { throw err; } }
      );
    } else {
      alert('Package weight must be greater than 0 and we just domestic shipping in FR');
    }
  }

  createShipment(formValue) {
    if (this.createShipmentForm.valid) {
      if (confirm('Are you sure to create shipping orders !!')) {
        const shipmentData = {
          quotePrice: this.quote.price,
          shipmentData: formValue
        };
        return this.shipmentService.createShipment(shipmentData).subscribe(
          res => {
            this.result = res;
            this.createShipmentForm.reset();
            this.quote = null;
          },
          err => { if (err) { throw err; } }
        );
      }
    }
  }

}
