import { Component, OnInit } from '@angular/core';
import { ShipmentService } from 'src/app/service/shipment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Message } from 'src/app/message';

@Component({
  selector: 'app-get-shipment',
  templateUrl: './get-shipment.component.html',
  styleUrls: ['./get-shipment.component.css']
})
export class GetShipmentComponent implements OnInit {
  message: Message;
  getShipmentForm = this.fb.group({
    contact: this.fb.group({
      name: [''],
      email: [''],
      phone: [''],
      countryCode: [''],
      locality: [''],
      postalCode: [''],
      address: [''],
    }),
    destination: this.fb.group({
      name: [''],
      email: [''],
      phone: [''],
      countryCode: [''],
      locality: [''],
      postalCode: [''],
      address: [''],
    }),
    package: this.fb.group({
      dimension: this.fb.group({
        height: [''],
        width: [''],
        length: [''],
        unit: [''],
      }),
      grossWeight: this.fb.group({
        amount: [''],
        unit: ['']
      })
    }),
    ref: ['']
  });
  constructor(private shipmentService: ShipmentService,
              private activateRoute: ActivatedRoute,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    const refId = +this.activateRoute.snapshot.paramMap.get('refId');
    this.getShipment(refId);
  }

  getShipment(refId) {
    if (refId) {
      return this.shipmentService.getShipment(refId).subscribe(
        res => this.hanleResponse(res)
      );
    } else { alert('Shipment not found !');}
  }

  deleteShipment() {
    if (confirm('Are you sure want to delete Shipment ?')) {
      const refId = +this.activateRoute.snapshot.paramMap.get('refId');
      return this.shipmentService.deleteShipment(refId).subscribe(
        res => {
          this.message = res;
          alert(this.message.message);
          return this.router.navigateByUrl('/createShipment');
        }
      );
    }
  }

  hanleResponse(res) {
    if (res === null) {
      alert('Shipment not found !');
      return this.router.navigateByUrl('/createShipment');
    } else {
      this.getShipmentForm.patchValue(res);
    }
  }
}
