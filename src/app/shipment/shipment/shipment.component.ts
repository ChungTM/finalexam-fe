import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ShipmentService } from 'src/app/service/shipment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.css']
})
export class ShipmentComponent implements OnInit {
  refId = new FormControl('');
  constructor(private shipmentService: ShipmentService,
              private route: Router) { }

  ngOnInit() {
  }
  getShipment() {
    const refId = +this.refId.value;
    if (refId) {
      return this.route.navigate(['getShipment/', refId]);
    } else {
      alert('Shipment not found !');
    }
  }
}
