export interface RefId {
    created_at: Date;
    ref: number;
    cost: number;
}
