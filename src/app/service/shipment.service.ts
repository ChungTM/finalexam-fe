import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Qoute } from '../qoute';
import { RefId } from '../ref-id';
import { Message } from '../message';
@Injectable({
  providedIn: 'root'
})
export class ShipmentService {
  readonly API_URL = 'http://localhost:3000';
  constructor(private http: HttpClient) { }
  createShipment(data) {
    return this.http.post<RefId>(`${this.API_URL}/create-shipment`, { data });
  }

  getQuote(quoteData) {
    return this.http.post<Qoute>(`${this.API_URL}/get-quote`, { quoteData });
  }

  getShipment(refId) {
    return this.http.get(`${this.API_URL}/getShipment/${refId}`);
  }

  deleteShipment(refId) {
    return this.http.delete<Message>(`${this.API_URL}/deleteShipment/${refId}`);
  }

}
