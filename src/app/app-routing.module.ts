import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateShipmentComponent } from './shipment/create-shipment/create-shipment.component';
import { GetShipmentComponent } from './shipment/get-shipment/get-shipment.component';

const routes: Routes = [
  {
    path: 'createShipment',
    component: CreateShipmentComponent
  }, {
    path: 'getShipment/:refId',
    component: GetShipmentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
